package com.zn.xzrpc.constant;

/**
 * Rpc框架常量
 */
public interface RpcConstant {

    /**
     * 默认配置前缀名
     */
    String DEFAULT_CONFIG_PREFIX = "rpc";
}
